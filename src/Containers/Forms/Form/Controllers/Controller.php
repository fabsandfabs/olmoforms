<?php

namespace Olmo\Forms\Containers\Forms\Form\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Olmo\Forms\App\Http\Controller\FormValidate;

use Olmo\Forms\Containers\Backoffice\Form\Models\Form;
use Olmo\Forms\App\Http\Controller\FormController;
use Olmo\Forms\App\Helpers\HelpersMailchimp;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Core\App\Helpers\HelpersExtra;
use Olmo\Core\App\Http\Controller\UploadController;

class Controller extends FormController
{

    use UploadController;

    public function getForm(Request $request)
    {

        $token = FormValidate::tokenChecker($request);
        if($token){
            return $token;
            exit();
        }

        $id = $request->id;
        $data = Db::table('olmo_form')->where('id', $id)->first();

        if($data){
            foreach ($data as $key => $value) {
                $type = HelpersData::getType($key);
                if ($type == 'formfield') {                
                    $fields = self::getFields($value);
                }            
            }  
    
            $form = json_decode(json_encode($data), true);
            $form = HelpersSerializer::cleanKeyObj($form);
            $form['fields'] = HelpersSerializer::cleanKeyArray($fields);
    
            // $form = HelpersSerializer::routeResponse();
    
            return response($form, 200);
        }

        return abort(404);
    }     

    public function sendForm(Request $request)
    {

        $token = FormValidate::tokenChecker($request);
        if($token){
            return $token;
        }                

        $ip = FormValidate::ipChecker($request);
        if($ip['server'] != null && $ip['user'] != null){
            return $ip;
        }        

        $rules = FormValidate::rules($request);
        if($rules){
            return $rules;
        }        

        return self::sendingFormData($request);
    }

    public function uploadChunks(Request $request)
    {
        return $this->uploadChunk();
    }

    public function uploadComplete(Request $request)
    {
        return $this->completeChunkManager($request);
    }    

    public function writeLead(Request $request)
    {
        $id = $request->input("id");
        $data = DB::table('olmo_form')->where('id', $id)->first();
        return HelpersMailchimp::writeLead($data, $request);
    }    

    public function checkLead(Request $request)
    {
        $id = $request->input("id");
        $data = DB::table('olmo_form')->where('id', $id)->first();
        return HelpersMailchimp::checkLead($data, $request);
    }

    public function updateLead(Request $request)
    {
        $id = $request->input("id");
        $data = DB::table('olmo_form')->where('id', $id)->first();
        return HelpersMailchimp::updateLead($data, $request);
    }

    public function unsubscribeLead(Request $request)
    {
        $id = $request->input("id");
        $data = DB::table('olmo_form')->where('id', $id)->first();
        return HelpersMailchimp::unsubscribeLead($data, $request);
    }

    public function addTagtoLead(Request $request)
    {
        $id = $request->input("id");
        $data = DB::table('olmo_form')->where('id', $id)->first();
        return HelpersMailchimp::addTagtoLead($data, $request);
    }

}
