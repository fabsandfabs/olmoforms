<?php

use Olmo\Forms\Containers\Forms\Form\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use Olmo\Forms\App\Helpers\HelpersDataForm;
use Olmo\Forms\App\Http\Controller\FormController;

Route::post('/test-template/{id}', function(Request $request){

    $id = $request->id;
    $type = isset($request->type) ? $request->type : 'main';
    $data = DB::table('olmo_form')->where('id', $id)->first();   
    $data = json_decode(json_encode($data, true), true);

    $content = HelpersDataForm::getFormData($request);
    $strings = FormController::getTranslations($data, $content);;
    $requestMerge = array_merge($strings, $content);
    $requestMerge = array_merge($requestMerge, $data);

    $setupEmailWithSmtp = [
        'email' => env('MAIL_FROM_ADDRESS'),
        'name' => env('MAIL_FROM_NAME')
    ];     

    if($type != 'main'){
        $template = DB::table('olmo_storage')->where('id', $data['subjectautoresponsetemplate_filemanager_setting'])->first();   
    } else {
        $template = DB::table('olmo_storage')->where('id', $data['subjecttemplate_filemanager_setting'])->first();   
    }

    $mail = new Olmo\Forms\App\Mail\Mailer($type, $data, $requestMerge, $template->filename, $setupEmailWithSmtp);
    return $mail->render();
});

// Get the list
Route::get('{token}/{id}', [Controller::class, 'getForm']);

Route::post('send/{token}/{id}', [Controller::class, 'sendForm']);

Route::post('uploadchunks', [Controller::class, 'uploadChunks']);

Route::post('uploadcomplete', [Controller::class, 'uploadComplete']);

// Mailchimp
Route::post('{token}/mailchimp/writelead', [Controller::class, 'writeLead']);
Route::post('{token}/mailchimp/checklead', [Controller::class, 'checkLead']);
Route::post('{token}/mailchimp/updatelead', [Controller::class, 'updatelead']);
Route::post('{token}/mailchimp/unsubscribelead', [Controller::class, 'unsubscribeLead']);
Route::post('{token}/mailchimp/addtagtolead', [Controller::class, 'addTagtoLead']);