<?php

namespace Olmo\Forms\App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Container\Container;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Markdown;
use Illuminate\Mail\Mailables\Attachment;

class Mailer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct($type, $data = [], $request = [], $template = '', $setupEmailWithSmtp)
    {
        $this->request = $request;
        $this->template = $template;
        $this->data = $data;
        $this->type = $type;
        $this->setupEmailWithSmtp = $setupEmailWithSmtp;
    }

    /**
     * Build the Markdown view for the message.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    protected function buildMarkdownView()
    {
        /** @var Markdown $markdown */
        $markdown = Container::getInstance()->make(Markdown::class);

        // use package resources path
        $markdown->loadComponentsFrom([
            storage_path('app/public/mail')
        ]);

        $data = $this->buildViewData();

        return [
            'html' => $markdown->render($this->markdown, $data),
            'text' => $this->buildMarkdownText($data),
        ];
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        $subject = isset($this->request['string.subject']) ? $this->request['string.subject'] : null;
        $subject = $subject ?? $this->data['subject_txt_setting'];

        $replysubject = isset($this->request['string.replysubject']) ? $this->request['string.replysubject'] : $this->data['subjectautoresponse_txt_setting'];

        $replyTo = $this->data['replyto_txt_setting'];

        return new Envelope(
            from: new Address(
                $this->setupEmailWithSmtp['email'], 
                $this->setupEmailWithSmtp['name']
            ),
            replyTo: [
                new Address($replyTo, config('mail.from.name'))
            ],
            subject: $this->type == 'main' ? $subject : $replysubject,
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        $template = explode('.', $this->template)[0];

        return new Content(
            markdown: $template,
            with: [
                'request' => $this->request
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachmentFile($path, $physic)
    {   
        $array = explode('/', $path);
        $array[count($array) - 1] = $physic;
        $filepath = implode('/',$array);
        
        return storage_path('app/public/media/' . $filepath);
    } 

    public function attachments(): array
    {
        
        $request = $this->request;
        
        if(
            env('FILE_ATTACHMENT') == 'true' && 
            $this->type = 'main' && 
            (isset($request['cv']) OR isset($request['attachment']) OR isset($request['file']))
        ){

            $file = $request['cv'] ?? $request['attachment'] ?? $request['file'];
            $filename = $request['cv_name'] ?? $request['attachment_name'] ?? $request['file_name'];

            //get the extension
            $ext = explode('.', $file);
            //get the name
            $name = explode('/', $filename);
            $name = $name[count($name)-1];

            return [Attachment::fromPath(self::attachmentFile($filename, $file))->as($name)->withMime('application/'.$ext[1])];

        } else {
            return [];
        }
    }
}