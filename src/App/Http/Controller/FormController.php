<?php

namespace Olmo\Forms\App\Http\Controller;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Config;

use Olmo\Forms\App\Mail\Mailer;
use Olmo\Core\App\Helpers\HelpersData;
use Olmo\Core\App\Helpers\HelpersSerializer;
use Olmo\Forms\App\Helpers\HelpersDataForm;
use Olmo\Forms\App\Helpers\HelpersMailchimp;
use Olmo\Forms\App\Helpers\HelpersBrevo;

class FormController extends Controller
{

    public static $translations;   

    public static function getFields($value)
    {
        if ($value == '') {
            return '';
        } else {
            $forms = explode(',', $value);
            foreach ($forms as $key => $item) {
                $forms[$key] = self::getField($item);
            }
            return $forms;
        }
    }

    public static function getField($value)
    {
        $form = DB::table('olmo_formfield')->where('id', $value)->first();
        
        if ($form) {    

            /**
             * Get value from Database Collection custom or default
             */
            if($form->element_select_general == 'inputlist'){

                $optiontype = DB::table('olmo_formfieldoptionstype')->where('fieldid_hidden_general', $value)->first();
                if($optiontype){
                    $table = 'olmo_'.$optiontype->formfieldoptionstype_select_general;
                    $istable = Schema::hasTable($table);
                    if($istable){
                        $name = $optiontype->formfieldoptionstype_select_general;                  
                        $model = ('App\Containers\Backoffice\\'.ucfirst($name).'\Models\\'.ucfirst($name));                    
    
                        $attribute = $model::theAttributes();

                        $orderfield = isset($attribute['orderlistget']) ? $attribute['orderlistget'] : 'name_txt_general';
                 
                        if($attribute['lang'] == true){
                            $getnation = Db::table($table)->where('locale_hidden_general', 'en')->where('enabled_is_general', 'true')->orderBy($orderfield, 'ASC')->get();   
                        } else {
                            $getnation = Db::table($table)->where('enabled_is_general', 'true')->orderBy($orderfield, 'ASC')->get();
                        }
                        if($getnation){
                            $getnation = HelpersSerializer::cleanKeyArray($getnation);
                            foreach($getnation as $key=>$nation){                           
                                $getnation[$key]['key'] = $nation['title'] ?? $nation['name'];
                                $getnation[$key]['value'] = $nation['name'];
                            }
                        }
        
                        $form->option_optionfield_general = $getnation;
                    }
                }

            }                   

            foreach ($form as $key => $item) {
                $type = HelpersData::getType($key);
                if ($type == 'optionfield') {
                    $form->$key = self::getOptions($item);
                } else if($type == 'checkbox'){
                    $form->$key = filter_var($item, FILTER_VALIDATE_BOOLEAN);
                }
            }
            return $form;
        } else {
            return '';
        }
    }

    public static function getOptions($value)
    {
        if(is_string($value) == true){
            $ids = explode(',', $value);
            $options = [];
            foreach ($ids as $id) {
                $option = DB::table('olmo_formfieldoptions')->where('id', $id)->first();
                if ($option) {
                    array_push($options, HelpersSerializer::cleanKeyObj($option));
                }
            }
            return $options;
        }
        return $value;
    }

    public static function sendingFormData($request, $type = 'route')
    {
        if($type == 'route'){
            $id = $request->id;
        } else {
            $id = $request['id'];
        }
        $data = DB::table('olmo_form')->where('id', $id)->first();        

        if(!$data){
            return response("No form found", 400);
        }        

        $formtypes = HelpersSerializer::multiselect($data->formtype_multid_general, 'formtype_multid_general', 0, '');
        $send = ['No type matches'];
        $send['code'] = 400;
        $csv = [];        
        foreach ($formtypes as $type) {
            if ($type['name'] == 'smtp') {
                $send = self::sendingEmailWithSmtp($data, $request);
            } 
            
            if ($type['name'] == 'postmark') {
                $send = self::sendingEmailWithPostmark($request);
            }
            
            if ($type['name'] == 'step') {
                $csv = self::writeRecordOnCsvForStep($request);
                $send = ['step'];
                $send['csv'] = $csv;
                $send['code'] = 200;
            } 

            if ($type['name'] == 'csv') {
                $csv = self::writeRecordOnCsv($request);
                $send = $send == ['No type matches'] ? 'csv' : $send;
                $send['csv'] = $csv;
                $send['code'] = 200;
            }             
            
            if ($type['name'] == 'brevo'){
                if($send['code'] = 200){
                    $brevo = HelpersBrevo::writeLead($data, $request, $type);
                    $send['brevo'] = $brevo;
                }  
            }

            if($type['name'] == 'dbwrite'){
                if($send['code'] = 200){
                    $dbwrite = HelpersDBWrite::writeLead($data, $request, $type);
                    $send['dbwrite'] = $dbwrite;
                }
            }
            
            if($type['name'] == 'mailchimp') {
                if($send['code'] = 200){
                    $mailchimp = HelpersMailchimp::writeLead($data, $request, $type);
                    $send['mailchimp'] = $mailchimp;
                }
            }
        }        
        return response($send, $send['code']);
    }

    public static function writeRecordOnCsvForStep($request)
    {
        $id = $request->id;
        $data = HelpersDataForm::getFormData($request);
        $stepId = $data['stepId'];

        $session_token = $request->header('X-CSRF-TOKEN');
        
        $array = [];
        foreach($data as $key=>$value) {
            array_push($array, $key);
            array_push($array, $value);
        }
        
        $uuid = (string)Str::uuid();
        
        $checkCsvfile = Db::table('olmo_storage')->where('truename', 'olmoemail/csv/'.$stepId.'-'.$session_token.'.csv')->first();
        $path = storage_path('app/public/media/olmoemail/csv/');
        $isDuplicate = false;
        if(!$checkCsvfile){
            $fp = fopen($path.$uuid.'.csv', 'w');
            fputcsv($fp, $array);
            fclose($fp);

            $csv_id = Db::table('olmo_storage')->insertGetid([
                'filename' => $uuid.'.csv',
                'truename' => 'olmoemail/csv/'.$stepId.'-'.$session_token.'.csv', 
                'model' => 'filemanager', 
                'type' => 'csv',
                'public' => 'false'
            ]);
        }else{
            $isDuplicate = HelpersDataForm::checkIfDataAreDuplicate($array, $path.$checkCsvfile->filename);
        }

        if($isDuplicate){
            $fp = fopen($path.$checkCsvfile->filename, 'a'); // va in errore filemanager
            fputcsv($fp, $array);
            fclose($fp);
        }
        
        $response = array(
            "fields" => $array,
            "FileCreated" => $checkCsvfile ? false : true,
            "FileWrited" => $isDuplicate ? false : true
        );

        return $response;

    }

    public static function writeRecordOnCsv($request)
    {
        $id = $request->id;
        $data = HelpersDataForm::getFormData($request);

        $formcsvfields = Db::table('olmo_form')->where('id', $id)->first();
        
        if($formcsvfields->csvfield_csvfield_csv != ''){
            
            $ids = explode(',', $formcsvfields->csvfield_csvfield_csv);            
            $arrayOrderField = [];
            foreach($ids as $i){
                $csvfield = Db::table('olmo_formcsvfield')->where('id', $i)->first();
                array_push($arrayOrderField, $csvfield);
            }            

            $array = [];
            foreach($arrayOrderField as $field){
                foreach($data as $key=>$value) {
                    if($key == $field->name){
                        array_push($array, $value);
                    }
                }
            }
            
            $uuid = (string)Str::uuid();
            
            $checkCsvfile = Db::table('olmo_storage')->where('truename', 'olmoemail/csv/form-'.$id.'.csv')->first();
            $path = storage_path('app/public/media/olmoemail/csv/');


            if($checkCsvfile){
                $fp = fopen($path.$checkCsvfile->filename, 'a');
            } else {
                $fp = fopen($path.$uuid.'.csv', 'w');
                Db::table('olmo_storage')->insertGetid([
                    'filename' => $uuid.'.csv',
                    'truename' => 'olmoemail/csv/form-'.$id.'.csv', 
                    'model' => 'filemanager', 
                    'type' => 'csv',
                    'public' => 'false'
                ]);
            }
    
            fputcsv($fp, $array);
    
            fclose($fp);

            $response = array(
                "fields" => $array,
                "create" => $checkCsvfile ? false : true,
                "check" => $checkCsvfile
            );

            return $response;
        }

        return false;

    }

    public static function setupEmailWithSmtp()
    {
        if (Schema::hasTable('olmo_formsmtp')) {
            $smtp = DB::table('olmo_formsmtp')->first();
            if ($smtp) //checking if table is not empty
            {
                $config = array(
                    'driver'     => 'smtp',
                    'host'       => $smtp->smtp_txt_general,
                    'port'       => (int)$smtp->port_txt_general,
                    'from'       => array('address' => $smtp->email_txt_general, 'name' => $smtp->displayname_txt_general),
                    'encryption' => strtoupper($smtp->tls_select_general),
                    'username'   => $smtp->username_txt_general,
                    'password'   => $smtp->password_pwd_general,
                    'sendmail'   => '/usr/sbin/sendmail -bs',
                    'pretend'    => true,
                );
                Config::set('mail', $config);

                return [
                    'email' => $smtp->email_txt_general,
                    'name' => $smtp->displayname_txt_general
                ];                
            }            
        }
        return [
            'from' => '',
            'name' => ''
        ];        
    }

    public static function setupEmailWithSmtpMulticonf($id)
    {
        if (Schema::hasTable('olmo_smtp')) {
            $smtp = DB::table('olmo_smtp')->where('id', $id)->first();
            if ($smtp)
            {
                $config = array(
                    'driver'     => 'smtp',
                    'host'       => $smtp->smtp_txt_general,
                    'port'       => (int)$smtp->port_txt_general,
                    'from'       => array('address' => $smtp->email_txt_general, 'name' => $smtp->displayname_txt_general),
                    'encryption' => strtoupper($smtp->tls_select_general),
                    'username'   => $smtp->username_txt_general,
                    'password'   => $smtp->password_pwd_general,
                    'sendmail'   => '/usr/sbin/sendmail -bs',
                    'pretend'    => true,
                );
                Config::set('mail', $config);

                return [
                    'email' => $smtp->email_txt_general,
                    'name' => $smtp->displayname_txt_general
                ];                
            }            
        }
        return [
            'from' => '',
            'name' => ''
        ];        
    }

    public static function sendingEmailWithSmtp($data, $request)
    {
        $template = DB::table('olmo_storage')->where('id', $data->subjecttemplate_filemanager_setting)->first();

        /**
         * Get the request body and convert it
         */
        $content = HelpersDataForm::getFormData($request);  
        /**
         * Get the string file and merge with the request body
         */
        $strings = self::getTranslations($data, $content);
        $requestMerge = array_merge($strings, $content);
        $types_ids = explode(',',$data->formtype_multid_general);
        $isFinalStep = in_array('32', $types_ids); // 32 is the id of final-step
        if(isset($content['stepId'])){
            if($content['stepId'] AND $isFinalStep){
                $steps = HelpersDataForm::getFormStepData($request);  
                $requestMerge = array_merge($steps, $requestMerge);
                $request['email'] = isset($steps['email']) ? $steps['email'] : self::checkEmailTo($request);
            }    
        }

        /**
         * Check if template is uploaded
         */
        if (!$template) {
            $sender = array(
                'email' => [
                    'sent' => false,
                    'to' => $data->a_txt_setting,
                    'cc' => $data->cc_txt_setting,
                    'bcc' => $data->bcc_txt_setting,
                    'replyto' => $data->replyto_txt_setting
                ],
                'code' => 400,
                'errors' => [
                    'Template missing'
                ]
            );

            return $sender;
        }

        if(env('SMTP_CONFIG', 'database') == 'database'){            
            if($data->smtp_id_general != ''){
                $setupEmailWithSmtp = self::setupEmailWithSmtpMulticonf($data->smtp_id_general);
            } else {
                $setupEmailWithSmtp = self::setupEmailWithSmtp();
            }
        } else {
            $setupEmailWithSmtp = [
                'email' => env('MAIL_FROM_ADDRESS'),
                'name' => env('MAIL_FROM_NAME')
            ];    
        }

        /**
         * Send the email 
         */

        if(isset($content['a'])){
            if($content['a'] == ''){
                $checkEmailTo = strpos($data->a_txt_setting, ',');
                $to = $checkEmailTo === false ? $data->a_txt_setting : explode(',', $data->a_txt_setting);
            } else {
                $checkEmailTo = strpos($content['a'], ',');
                $to = $checkEmailTo === false ? $content['a'] : explode(',', $content['a']);
            }
        } else {
            $checkEmailTo = strpos($data->a_txt_setting, ',');
            $to = $checkEmailTo === false ? $data->a_txt_setting : explode(',', $data->a_txt_setting);
        }

        $checkEmailCc = strpos($data->cc_txt_setting, ',');
        $cc = $checkEmailCc === false ? $data->cc_txt_setting : explode(',', $data->cc_txt_setting);

        $checkEmailBcc = strpos($data->bcc_txt_setting, ',');
        $bcc = $checkEmailBcc === false ? $data->bcc_txt_setting : explode(',', $data->bcc_txt_setting);

        // return ['main', gettype((array)$data), gettype($requestMerge), $template->filename];
        try {
            Mail::to($to)
                ->cc($cc)
                ->bcc($bcc)
                ->send(new Mailer('main', (array)$data, $requestMerge, $template->filename, $setupEmailWithSmtp));

            $send = true;
        
        } catch (Exception $ex) {
            $send = false;
        }

        /**
         * Send the auto-reply
         * First check if the template is set then send the reply 
         */
        $templateautoreply = DB::table('olmo_storage')->where('id', $data->subjectautoresponsetemplate_filemanager_setting)->first();

        $reply = false;
        if ($templateautoreply) {
            $email = self::checkEmailTo($request);            

            try {
                Mail::to($email)
                    ->send(new Mailer('reply', (array)$data, $requestMerge, $templateautoreply->filename, $setupEmailWithSmtp));
    
                $reply = true;
            } catch (Exception $ex) {
                
            }
        }

        if ($send) {
            $sender = self::theResponse($requestMerge, $data, $templateautoreply, $reply, 200, [], $to);
            self::generateLog($requestMerge, $data, $send, $templateautoreply, $reply, $to, 200);
            if($isFinalStep){
                self::deleteCsvSteps($request, $content['stepId']);
            }
        } else {
            $sender = self::theResponse($requestMerge, $data, $templateautoreply, $reply, 400, $send, $to);
            self::generateLog($requestMerge, $data, $send, $templateautoreply, $reply, $to, 400);
        }


        return $sender;
    }

    public static function deleteCsvSteps($request, $stepId){
        if(is_array($request)){
            $session_token = $request['X-CSRF-TOKEN'];
        } else {
            $session_token = $request->header('X-CSRF-TOKEN');
        }

        $checkCsvfile = Db::table('olmo_storage')->where('truename', 'like', '%'.$session_token.'.csv')->get();

        if($checkCsvfile){
            foreach($checkCsvfile as $file) {
                Storage::disk('local')->delete('public/media/olmoemail/csv/'.$file->truename);
                Db::table('olmo_storage')->where('truename', $file->truename)->delete();
            }
        }
    }

    public static function checkEmailTo($request)
    {
        if(isset($request['email'])){
            $email = $request['email'];
        } else if(isset($request['email'])) {
            $email = $request->email;
        } else {
            $email = '';
        }
        return $email;
    }

    public static function theResponse($request, $data, $templateautoreply, $reply, $code, $errors, $to)
    {

        return array(
            'email' => [
                'sent' => true,
                'to' => $to,
                'cc' => $data->cc_txt_setting,
                'bcc' => $data->bcc_txt_setting,
                'replyto' => $data->replyto_txt_setting,
                'reply' => $reply ? 'sent' : 'Not sent'
            ],
            'form' => [
                'id' => $data->id,
                'body' => $request
            ],
            'code' => $code,
            'errors' => $errors
        );
    }

    public static function generateLog($request, $data, $sender, $templateautoreply, $reply, $to, $code)
    {
        $log = self::theResponse($request, $data, $templateautoreply, $reply, $code, [], $to);
        Log::channel('forms')->info(json_encode($log));
        self::generateLogDatabase($data, $code);
    }

    public static function generateLogDatabase($data, $code)
    {
        DB::table('olmo_formlog')->insert(['name' => $data->name_txt_general, 'date' => Carbon::now(), 'status' => $code]);
    }

    public function sendingEmailWithPostmark($request)
    {
    }

    public static function getTranslations($stringfile, $request): array
    {

        if (self::$translations) {
            return self::$translations;
        }
        
        if(isset($stringfile->strings_filemanager_setting)){
            $stringfile = json_decode(json_encode($stringfile, true), true);
            if($stringfile['strings_filemanager_setting'] == ""){
                return [];
            }
        }
        if(isset($stringfile['strings_filemanager_setting'])){
            if($stringfile['strings_filemanager_setting'] == ""){
                return [];
            }
        }

        $file = DB::table('olmo_storage')->where('id', $stringfile['strings_filemanager_setting'])->first();
        $filepath = explode('/', $file->truename);
        $filepath[count($filepath) - 1] = $file->filename;
        $filename = implode('/', $filepath);

        $data = [];
        $locale = $request['locale'];
        $filepath = storage_path('app/public/media/' . $filename);
        if (!file_exists($filepath)) {
            exit('Olmo: missing translation file. '. $filepath);
        }

        $file = file($filepath);
        $localeIdx = 0;

        // grab first line
        if (isset($file[0])) {
            $firstRow = str_getcsv($file[0]);
            // loop through first line first row's cells
            foreach ($firstRow as $column) {
                // get the right column index and break
                if (trim($column) === $locale) {
                    break;
                }
                $localeIdx++;
            }
        }

        // now loop through all lines and associate the right key to the right
        // localised string
        foreach ($file as $line) {
            $row = str_getcsv($line);

            // try to get the correct one
            if (isset($row[$localeIdx])) {
                $data[$row[0]] = trim($row[$localeIdx]);
            }
            // fallback to default language one (the first column, to ease the
            // process during development)
            elseif (isset($row[1])) {
                $data[$row[0]] = trim($row[1]);
            }
            // otherwise return a helpful string to detect the missing
            // translation
            else {
                $data[$row[0]] = 'Untranslated key "' . $row[0] . '"';
            }
        }

        self::$translations = $data;

        return $data;
    }
}
