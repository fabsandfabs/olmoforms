<?php

namespace Olmo\Forms\App\Http\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Olmo\Forms\App\Helpers\HelpersDataForm;

class FormValidate extends Controller
{    

    public static function ipChecker($request)
    {
        $server = self::ipServerChecker($request);
        $user = self::ipUserChecker($request);        

        return array(
            "server" => $server,
            "user" => $user
        );        

    }

    public static function ipServerChecker($request)
    {
        $ip = request()->server('REMOTE_ADDR');
        $ipfield = Db::table('olmo_formauth')->where('id', '1')->first();
        $ipallowed = explode(",", $ipfield->ipaddressforserver_txtarea_general);
        if(!in_array($ip, $ipallowed)){

            return array([
                'email'    => false,
                'code'     => 400,
                'ip'       => $ip,
                'message'  => 'Validation errors',
                'errors'   => 'Ip not allowed'
            ]);

        }

        return null;

    }

    public static function ipUserChecker($request)
    {
        $ip = $request->ip();
        $ipfield = Db::table('olmo_formauth')->where('id', '1')->first();
        $ipallowed = explode(",", $ipfield->ipuseraddressblacklist_txtarea_general);
        if(in_array($ip, $ipallowed)){

            return array([
                'email'    => false,
                'code'     => 400,
                'ip'       => $ip,
                'message'  => 'Validation errors',
                'errors'   => 'User Ip not allowed'
            ]);

        }    

        return null;

    }    

    public static function tokenChecker($request)
    {
        $token = $request->token;
        $tokenfield = Db::table('olmo_formauth')->where('id', '1')->first();
        $tokenfield = $tokenfield->token_txtdis_general;
        if($token != $tokenfield){

            return array([
                'email'    => false,
                'code'     => 400,
                'ip'       => $token,
                'message'  => 'Validation errors',
                'errors'   => 'Token error'
            ]);

        }    

        return null;

    }    

    public static function rules($request)
    {
        $fieldToValidate = self::fieldToValidate($request);

        $validator = Validator::make($request->all(), $fieldToValidate, self::messages());
 
        if ($validator->fails()) {
            return self::failedValidation($validator);
        }

    }

    public static function failedValidation($validator)
    {
        return response()->json([
            'email'     => false,
            'code'      => 400,
            'message'   => 'Validation errors',
            'errors'    => $validator->errors()
        ], 400);
    }
    
    public static function messages()
    {
        return [
            'required' => 'The :attribute field is required.',
            'email.email' => 'Email is not correct'
        ];
    }

    public static function fieldToValidate($request)
    {
        $id = $request->id;
        $body = HelpersDataForm::getFormData($request);

        $fields = DB::table('olmo_formfield')->where('postid_hidden_general', $id)->get();

        $errors = [];
        foreach($fields as $key=>$value){           
            // if(self::checkDefaultKey($key, $value)){
                if($value->required_is_general == 'true'){
                    if($value->typology_select_general == 'email'){
                        $errors[$value->name_txt_general] = 'required|email';
                    } else {
                        if($value->dependentfield_select_general != ''){
                            $profession = $value->dependentfield_select_general;
                            $designer = $body[$profession];
                            if($designer == $value->dependenttrigger_txt_general){
                                $errors[$value->name_txt_general] = 'required';
                            }
                        } else {
                            $errors[$value->name_txt_general] = 'required';
                        }
                    }
                }
            // }
        }

        return $errors;

    }    

    public static function checkDefaultKey($key)
    {
        if($key == 'a' || $key == 'locale'){
            return false;
        }
        return true;
    }    
}