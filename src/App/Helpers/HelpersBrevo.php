<?php

namespace Olmo\Forms\App\Helpers;

use Olmo\Core\App\Helpers\HelpersSerializer;

use Brevo\Client\Configuration;
use Illuminate\Support\Facades\File;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Request;

class HelpersBrevo
{ 

    private const CONTAINERS_DIRECTORY_NAME = 'app/Settings';

    private const CONTAINER_BREVO = 'Brevo';

    public static function getMap(){

        $brevoDirectory = base_path(self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . self::CONTAINER_BREVO . DIRECTORY_SEPARATOR);
        $directory = File::isDirectory($brevoDirectory);

        if ($directory) {
            $brevoRequiredPath = $brevoDirectory . 'brevoattributes.json';
            if($brevoRequiredPath){
                $databrevo = json_decode(file_get_contents($brevoRequiredPath), true);
                return $databrevo;
            }         

        }    
        
        return [];

    }    

    /**
     * This is the main method to write contact in mailchimp
     * obviously the configuration in the backoffice must be done correctly
     * the script detect the presence of a tag in the configuration and adds the tag at row 88
     */
    public static function writeLead($data, $request, $type = 'route')
    {        

        $config = Configuration::getDefaultConfiguration()->setApiKey('api-key', env('BREVO_API_KEY'));

        $apiInstance = new \Brevo\Client\Api\ContactsApi(
            // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
            // This is optional, `GuzzleHttp\Client` will be used as default.
            new \GuzzleHttp\Client(),
            $config
        );                    

        $attributes = self::getMap();

        $array = [];
        foreach($attributes['attributes'] as $item){
            if($request[$item['fieldolmo']] == 'true' OR $request[$item['fieldolmo']] == 'false'){
                if($request[$item['fieldolmo']] == 'true'){
                    $array[$item['fieldbrevo']] = true;
                } else {
                    $array[$item['fieldbrevo']] = false;
                }
            } else {
                $array[$item['fieldbrevo']] = $request[$item['fieldolmo']];
            }
        }
            
        $obj = (object)$array;

        $ids = [];
        foreach($attributes['ids'] as $item){
            if(@$request['locale'] == $item['locale'] AND @$request['newsletter'] == 'true'){
                array_push($ids, $item['id']);
            }
        }

        $createContact = new \Brevo\Client\Model\CreateContact([
            'email' => $request['email'],
            'updateEnabled' => true,
            'attributes' => $obj,
            'listIds' => count($ids) > 0 ? $ids : [1]
       ]); // \Brevo\Client\Model\CreateContact | Values to create a contact

       try {
            $result = [
                'response' => $apiInstance->createContact($createContact),
                'object' => [
                    'email' => $request['email'],
                    'updateEnabled' => true,
                    'attributes' => $obj,
                    'listIds' => count($ids) > 0 ? $ids : [1]                    
                ]
            ];
        } catch (Exception $e) {
            $result = ['Exception when calling ContactsApi->createContact: ', $e->getMessage(), PHP_EOL];
        }               

        return response(['response' => $result], 200);
    }

}