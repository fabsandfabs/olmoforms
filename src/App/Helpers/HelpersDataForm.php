<?php

namespace Olmo\Forms\App\Helpers;
use Illuminate\Support\Facades\DB;

class HelpersDataForm
{

    public static function getFormData($request)
    {
        /**
         * Check twice this rule there is something odd
         */
        if(!is_array($request)){
            $body = $request->getContent();
        } else {
            $body = json_encode($request);
        }

        if($body && !is_array($request)){
            $body = $request->all();
        } else {
            $body = json_decode($body, true);            
        }

        return $body;
        
    }

    public static function getFormStepData($request)
    {
      if(is_array($request)){
        $session_token = $request['X-CSRF-TOKEN'];
      }else{
        $session_token = $request->header('X-CSRF-TOKEN');
      }

      $checkCsvfile = Db::table('olmo_storage')->where('truename', 'like', '%'.$session_token.'.csv')->get();
      $path = storage_path('app/public/media/olmoemail/csv/');

      $stepBlobs = [];
      if($checkCsvfile->count() == 1){
        $step = $checkCsvfile[0];
        $stepBlobs = self::groupByColumnPairs($path.$step->filename);
      } else {
        foreach($checkCsvfile as $step){
          $array = self::groupByColumnPairs($path.$step->filename);
          $stepBlobs = array_merge($array, $stepBlobs);
        }
      }
      
      return $stepBlobs;
    }

    static function groupByColumnPairs($filePath) {
        $data = [];
        $handle = fopen($filePath, "r+");
      
        if ($handle !== false) {
          while (($row = fgetcsv($handle, 0, ",")) !== false) {
            $rowData = [];
            for ($i = 0; $i < count($row); $i += 2) {
              $rowData[$row[$i]] = $row[$i + 1];
            }
            $data = array_merge($data, $rowData);
          }
          fclose($handle);
        } else {

          echo "Error opening file: $filePath";
        }
      
        return $data;
    }

    public static function checkIfDataAreDuplicate($data, $csv){
      $fp = fopen($csv, 'r+');

      while ($row = fgetcsv($fp)) {
          $fieldsCsv = array_diff($data, $row);
          if($fieldsCsv == []){
            fclose($fp);
            return true;
          }
      }
      fclose($fp);
      return false;
      
    }
            


}