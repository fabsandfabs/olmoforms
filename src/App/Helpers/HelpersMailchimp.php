<?php

namespace Olmo\Forms\App\Helpers;

use Olmo\Core\App\Helpers\HelpersSerializer;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Request;

class HelpersMailchimp
{

    public static function checkConnectionToMailchimp($data, $request)
    {

        $formtypes = HelpersSerializer::multiselect($data->mailchimp_multid_general, 'mailchimp_multid_general', 0, '');
        
        $mailchimp = new \MailchimpMarketing\ApiClient();
        
        $mailchimp->setConfig([
            'apiKey' => $formtypes[0]['apikey'],
            'server' => $formtypes[0]['server']
        ]);

        return [
            'mailchimp' => $mailchimp,
            'formtypes' => $formtypes
        ];

    }

    public static function getListId($data, $lang)
    {
        foreach($data as $list){
            if($lang == $list['fieldvalue']){
                $list_id = $list['listvalue'];
            }
        }

        return $list_id;
    }

    public static function responseMailchimp($e)
    {
        $response = $e->getResponse();
        $responseBodyAsString = $response->getBody()->getContents();
        $response = json_decode($responseBodyAsString, true);
        return response($response, $response['status']);
    }

    public static function checkLead($data, $request, $type)
    {
        $mailchimpResponse = self::checkConnectionToMailchimp($data, $request);
        $mailchimp = $mailchimpResponse['mailchimp'];
        $formtypes = $mailchimpResponse['formtypes'];   
        
        if($type == 'route'){
            $lang = $request->input('locale');
            $email = $request->input('email');
        }else{
            $lang = $request['locale'];
            $email = $request['email'];
        }

        $list_id = self::getListId($formtypes[0]['mailchimplistid'], $lang);;
        $subscriber_hash = md5(strtolower($email));

        try {
            $response = $mailchimp->lists->getListMember($list_id, $subscriber_hash);
            return response(json_decode(json_encode($response), true), 200);
        } catch (ClientException $e) {
            return self::responseMailchimp($e);
        }        
        
    }    

    /**
     * This is the main method to write contact in mailchimp
     * obviously the configuration in the backoffice must be done correctly
     * the script detect the presence of a tag in the configuration and adds the tag at row 88
     */
    public static function writeLead($data, $request, $type = 'route')
    {
        $mailchimpResponse = self::checkConnectionToMailchimp($data, $request);
        $mailchimp = $mailchimpResponse['mailchimp'];
        $formtypes = $mailchimpResponse['formtypes'];

        if($type == 'route'){
            $lang = $request->input('locale');
            $email = $request->input('email');
            $conditionFiled = $request->input($formtypes[0]['fieldname']);
        } else {
            $lang = $request['locale'];
            $email = $request['email'];
            $conditionFiled = $request[($formtypes[0]['fieldname'])];
        }

        $conditionValue = $formtypes[0]['fieldvalue'];

        if($conditionFiled != $conditionValue){
            return response([], 404);
        }

        $doubleoptin = $formtypes[0]['doubleoptin'];
        $status = $doubleoptin == "true" ? "pending" : "subscribed";
        
        $response = $mailchimp->ping->get();

        $list_id = self::getListId($formtypes[0]['mailchimplistid'], $lang);

        $fields = [
            "email_address" => $email,
            "status" => $status
        ];        

        $mergefields = [];
        $tags = [];
        foreach($formtypes[0]['mailchimpfield'] as $list){
            if($list['thisemail'] == 'false' AND $list['thistag'] == 'false'){
                if($type == 'route'){
                    $mergefields[$list['mailchimpfieldname']] = $request->input($list['fieldname']);
                }else{
                    $mergefields[$list['mailchimpfieldname']] = $request[($list['fieldname'])];
                }
            } else if($list['thistag'] == 'true') {
                if($type == 'route'){
                    $tag = ["name" => $request->input($list['fieldname']), "status" => "active"];
                }else{
                    $tag = ["name" => $request[($list['fieldname'])], "status" => "active"];
                }                
                array_push($tags, $tag);
            }
        }

        if(count($mergefields) > 0){
            $fields['merge_fields'] = $mergefields;
        }

        /** 
         * Check if email exist in the DB 
         * If so update the lead, if not go ahead and write it
         * */
        $check = self::checkLead($data, $request, $type);
        if($check->getStatusCode() == '200'){
            $response = self::updateLeadRequest($mailchimp, $list_id, $email, $fields);;
        } else {
            $response = self::writeLeadRequest($mailchimp, $list_id, $fields);
        }

        $tagResponse = null;
        if($tags){
            $tagResponse = self::addTagtoLeadRequest($mailchimp, $list_id, $email, $tags);
        }
        
        return response(['response' => $response, 'tags' => $tagResponse], 200);

    }

    public static function updateLead($data, $request)
    {
        $mailchimpResponse = self::checkConnectionToMailchimp($data, $request);
        $mailchimp = $mailchimpResponse['mailchimp'];
        $formtypes = $mailchimpResponse['formtypes'];

        $lang = $request->input('locale');
        $email = $request->input('email');
        
        $response = $mailchimp->ping->get();

        $list_id = self::getListId($formtypes[0]['mailchimplistid'], $lang);        

        $fields = [
            "email_address" => $email
        ];        

        $mergefields = [];
        $tags = [];
        foreach($formtypes[0]['mailchimpfield'] as $list){
            if($list['thisemail'] == 'false' AND $list['thistag'] == 'false'){
                $mergefields[$list['mailchimpfieldname']] = $request->input($list['fieldname']);
            } else if($list['thistag'] == 'true') {
                $tag = ["name" => $request->input($list['fieldname']), "status" => "active"];
                array_push($tags, $tag);
            }
        }

        if(count($mergefields) > 0){
            $fields['merge_fields'] = $mergefields;
        }

        $response = self::updateLeadRequest($mailchimp, $list_id, $email, $fields);
        
        $tagResponse = null;
        if($tag){
            $tagResponse = self::addTagtoLeadRequest($mailchimp, $list_id, $email, $tags);
        }
        
        return response(['response' => $response, 'tags' => $tagResponse], 200);
    }

    public static function unsubscribeLead($data, $request)
    {
        $mailchimpResponse = self::checkConnectionToMailchimp($data, $request);
        $mailchimp = $mailchimpResponse['mailchimp'];
        $formtypes = $mailchimpResponse['formtypes'];   
        
        $lang = $request->input('locale');
        $email = $request->input('email'); 

        $list_id = self::getListId($formtypes[0]['mailchimplistid'], $lang);;
        $subscriber_hash = md5(strtolower($email));
        
        try {
            $response = $mailchimp->lists->updateListMember($list_id, $subscriber_hash, ["status" => "unsubscribed"]);
            return response(json_decode(json_encode($response), true), 200);
        } catch (ClientException $e) {
            return self::responseMailchimp($e);
        }
        
    }

    public static function addTagtoLead($data, $request)
    {
        $mailchimpResponse = self::checkConnectionToMailchimp($data, $request);
        $mailchimp = $mailchimpResponse['mailchimp'];
        $formtypes = $mailchimpResponse['formtypes'];   
        
        $lang = $request->input('locale');
        $email = $request->input('email'); 
        $tag = $request->input('tag'); 

        $list_id = self::getListId($formtypes[0]['mailchimplistid'], $lang);

        $response = self::addTagtoLeadRequest($mailchimp, $list_id, $email, $tag);
        
        return response(json_decode(json_encode($response), true), 200);
    }

    public static function writeLeadRequest($mailchimp, $list_id, $fields)
    {        
        try {
            $response = $mailchimp->lists->addListMember($list_id, $fields);
            return $response;
        } catch (ClientException $e) {
            return self::responseMailchimp($e);
        }
    }

    public static function updateLeadRequest($mailchimp, $list_id, $email, $fields)
    {        
        $subscriber_hash = md5(strtolower($email));

        try {
            $response = $mailchimp->lists->updateListMember($list_id, $subscriber_hash, $fields);
            return $response;
        } catch (ClientException $e) {
            return self::responseMailchimp($e);
        }
    }    

    public static function addTagtoLeadRequest($mailchimp, $list_id, $email, $tag)
    {
        $tags = ["tags" => $tag];
        $subscriber_hash = md5(strtolower($email));
        
        try {
            $response = $mailchimp->lists->updateListMemberTags($list_id, $subscriber_hash, $tags);
            return $response;
        } catch (ClientException $e) {
            return self::responseMailchimp($e);
        }   

    }    

}