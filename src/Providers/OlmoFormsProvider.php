<?php

namespace Olmo\Forms\Providers;

use Olmo\Forms\Foundation\OlmoForms;
use Illuminate\Support\ServiceProvider as LaravelAppServiceProvider;

class OlmoFormsProvider extends LaravelAppServiceProvider
{

    public function boot(): void
    {

    }

    public function register(): void
    {
        parent::register();
        
        // Register Core Facade Classes, should not be registered in the $aliases property, since they are used
        // by the auto-loading scripts, before the $aliases property is executed.
        $this->app->alias(OlmoForms::class, 'OlmoForms');
    }

}
