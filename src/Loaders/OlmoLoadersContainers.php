<?php

namespace Olmo\Forms\Loaders;

use Illuminate\Support\Facades\File;

class OlmoLoadersContainers
{

    private const CONTAINERS_DIRECTORY_NAME = 'Containers';

    // Below is the default migration tacking action, 
    // user cannot takes control of this cause is the minimum set up to run the Backoffice

    public static function getAllDefaultContainerPaths(): array
    {        
        $sectionNames = self::getDefaultSectionNames();        
        $containerPaths = [];
        foreach ($sectionNames as $name) {
            $sectionContainerPaths = self::getDefaultSectionContainerPaths($name);
            foreach ($sectionContainerPaths as $containerPath) {
                $containerPaths[] = $containerPath;
            }
        }
        
        return $containerPaths;
    }    
    
    public static function getDefaultSectionNames(): array
    {
        $sectionNames = [];        
        foreach (self::getDefaultSectionPaths() as $sectionPath) {
            $sectionNames[] = basename($sectionPath);
        }
    
        return $sectionNames;
    }    
    
    public static function getDefaultSectionPaths(): array
    {                
        return File::directories(__DIR__ . '/../' . self::CONTAINERS_DIRECTORY_NAME);
    }
    
    public static function getDefaultSectionContainerPaths(string $sectionName): array
    {        
        return File::directories(__DIR__ . '/../' . self::CONTAINERS_DIRECTORY_NAME . DIRECTORY_SEPARATOR . $sectionName);
    }    

}